use crossterm::event::{Event, KeyCode, KeyEvent};
use serde::{Deserialize, Serialize};

use crate::config::Config;
use crate::state::State;
use crate::tui::component::{Component, Handler};
use crate::tui::TuiState;

#[derive(Debug, Clone, Default)]
pub struct Controller {
    config: Config,
}

impl Controller {
    pub fn new(config: Config) -> Self {
        Self { config }
    }
    pub async fn handle(&self, event: &Event, state: &mut State, tui: &mut TuiState) {
        match event {
            Event::Key(key) => self.handle_key_event(key, state, tui).await,
            Event::FocusGained
            | Event::FocusLost
            | Event::Mouse(_)
            | Event::Paste(_)
            | Event::Resize(_, _) => {}
        }
    }
    pub async fn handle_key_event(&self, event: &KeyEvent, state: &mut State, tui: &mut TuiState) {
        let action = match event.code {
            KeyCode::Esc | KeyCode::Char('q') => Action::Quit,
            KeyCode::Char('j') => Action::Next,
            KeyCode::Char('k') => Action::Previous,
            KeyCode::Tab => Action::Toggle(Toggle::Collapsed),
            KeyCode::Char('w') => Action::Toggle(Toggle::Rendered),
            KeyCode::Char('@') => Action::ToggleBar,
            _ => {
                tracing::debug!("Unhandled key event: {event:?}");
                Action::NoOp
            }
        };

        self.handle_action(&action, event, state, tui).await;
    }
    pub async fn handle_action(
        &self,
        action: &Action,
        _event: &KeyEvent,
        state: &mut State,
        tui: &mut TuiState,
    ) {
        match &action {
            Action::Quit => {
                state
                    .bus()
                    .send_controller_msg(action.clone())
                    .await
                    .unwrap();
            }
            Action::ToggleBar => tui.toggle_footer(),

            #[allow(unreachable_patterns)]
            _ => {
                tracing::debug!("Unhandled action: {action:?}");
                self.focused(state)
                    .handle_action(action, _event, state, tui)
                    .await;
            }
        }
    }
    /// Component that should handle action input
    pub fn focused(&self, state: &State) -> Component {
        Component::SuggestionList(state.suggestions.clone())
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
/// A user action, that can be triggered
pub enum Action {
    Quit,
    NoOp,
    Next,
    Previous,
    Toggle(Toggle),
    ToggleBar,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
/// A user action, that can be triggered
pub enum Toggle {
    Collapsed,
    Rendered,
}
