use tokio::sync::mpsc::{self, Receiver, Sender};
use tokio::task::JoinHandle;

use crate::controller::Action;
use crate::integration::Suggestion;

/// Shared capacity of the mpsc channels
const MPSC_CHANNEL_CAP: usize = 32;

/// A bus for sending messages between threads,
/// Also handles spawning the workers.
pub(crate) struct SystemBus {
    command_tx: Sender<Suggestion>,
    // command_worker_handle: Option<JoinHandle<()>>,
    // nix_tx: mpsc::Sender<NixEvalMsg>,
    // nix_rx: Option<Receiver<NixEvalMsg>>,
    // nix_result_tx: mpsc::Sender<NixResultMsg>,
    // nix_worker_handle: Option<tokio::task::JoinHandle<()>>,
    controller_tx: Sender<Action>,
}

/// Handles the result messages in the main thread.
pub(crate) struct ResultHandler {
    controller_rx: Option<Receiver<Action>>,
    command_rx: Option<Receiver<Suggestion>>,
}

impl ResultHandler {
    pub(crate) fn new(controller_rx: Receiver<Action>, command_rx: Receiver<Suggestion>) -> Self {
        Self {
            controller_rx: Some(controller_rx),
            command_rx: Some(command_rx),
        }
    }

    /// Takes ownership of the receiver, leaving `None` in its place.
    pub(crate) fn controller_rx(&mut self) -> Option<Receiver<Action>> {
        self.controller_rx.take()
    }
    /// Takes ownership of the receiver, leaving `None` in its place.
    pub(crate) fn command_rx(&mut self) -> Option<Receiver<Suggestion>> {
        self.command_rx.take()
    }
}

impl SystemBus {
    pub(crate) fn new() -> (Self, ResultHandler) {
        // let (nix_tx, nix_rx) = mpsc::channel::<NixEvalMsg>(MPSC_CHANNEL_CAP);
        // let (nix_result_tx, nix_result_rx) = mpsc::channel::<NixResultMsg>(MPSC_CHANNEL_CAP);
        let (command_tx, command_rx) = mpsc::channel::<Suggestion>(MPSC_CHANNEL_CAP);

        let (controller_tx, controller_rx) = mpsc::channel::<Action>(MPSC_CHANNEL_CAP);

        (
            Self {
                controller_tx,
                command_tx,
            },
            ResultHandler::new(controller_rx, command_rx),
        )
    }

    /// Initializes the system bus.
    ///
    /// *Panics:* If it is called more than once.
    /// TODO: Use a OnceCell
    pub fn init(&mut self) {
        // let mut nix_rx = self.nix_rx.take().unwrap();
        // let nix_result_tx = self.nix_result_tx.clone();
        //
        // let mut we_rx = self.we_rx.take().unwrap();
        // let we_result_tx = self.we_result_tx.clone();

        // let nix_worker_handle = tokio::spawn(async move {
        //     let nix_worker = NixEvalWorker::new(nix_result_tx, flake_ref);
        //     loop {
        //         if let Some(msg) = nix_rx.recv().await {
        //             nix_worker.handle(msg).await;
        //         }
        //     }
        // });
        //
        // let web_worker_handle = tokio::spawn(async move {
        //     let mut web_worker = WebWorker::new(we_result_tx);
        //     loop {
        //         if let Some(msg) = we_rx.recv().await {
        //             web_worker.handle(msg).await;
        //         }
        //     }
        // });

        // self.nix_worker_handle = Some(nix_worker_handle);
        // self.web_worker_handle = Some(web_worker_handle);
        tracing::debug!("Initialized SystemBus.");
    }

    // pub async fn send_nix_message(
    //     &self,
    //     msg: NixEvalMsg,
    // ) -> Result<(), mpsc::error::SendError<NixEvalMsg>> {
    //     self.nix_tx.send(msg).await
    // }
    //
    // pub async fn send_web_message(
    //     &self,
    //     msg: WebEvalMsg,
    // ) -> Result<(), mpsc::error::SendError<WebEvalMsg>> {
    //     self.we_tx.send(msg).await
    // }
    //
    // pub async fn send_flk_message(
    //     &self,
    //     msg: FlkMsg,
    // ) -> Result<(), mpsc::error::SendError<FlkMsg>> {
    //     self.flk_tx.send(msg).await
    // }
    pub async fn send(&self, msg: BusMsg) -> Result<(), mpsc::error::SendError<Action>> {
        match msg {
            BusMsg::Action(action) => self.send_controller_msg(action).await,
        }
    }
    pub async fn send_controller_msg(
        &self,
        msg: Action,
    ) -> Result<(), mpsc::error::SendError<Action>> {
        self.controller_tx.send(msg).await
    }

    pub fn controller_tx(&self) -> Sender<Action> {
        self.controller_tx.clone()
    }
    pub fn command_tx(&self) -> Sender<Suggestion> {
        self.command_tx.clone()
    }
    // pub fn nix_tx(&self) -> mpsc::Sender<NixEvalMsg> {
    //     self.nix_tx.clone()
    // }
}

#[derive(Debug, Clone)]
pub enum BusMsg {
    Action(Action),
}

impl Drop for SystemBus {
    fn drop(&mut self) {
        tracing::debug!("Dropped SystemBus.");
    }
}
