#[derive(Default, Debug, Clone)]
pub struct Document {
    pub rope: ropey::Rope,
}

impl Document {
    pub fn new(rope: ropey::Rope) -> Self {
        Self { rope }
    }
    pub fn from(text: &str) -> Self {
        let rope = ropey::Rope::from(text);
        Self::new(rope)
    }
    pub async fn open(location: &str) -> Self {
        let bytes = tokio::fs::read(location).await.unwrap();
        let string = std::str::from_utf8(&bytes).unwrap();
        Self::from(string)
    }
}
