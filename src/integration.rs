//! This is supposed to be generic over multiple lsp and linting backends
//! Currently most functionality here is hardcoded for clippy.
//!
//!
//!
//!
//!
//!
//! structure:
pub(crate) mod commands;
pub(crate) mod queue;

use serde::{Deserialize, Serialize};
use tokio::sync::mpsc::Sender;

use self::commands::clippy::{ClippyChildren, Code, CompilerMessageSpan};
use self::commands::cmd::FixmeCmd;

pub trait CmdIntegration {
    /// Convert a line into a [`Suggestion`]
    fn from_line(line: &str) -> Option<Suggestion>;

    fn cmd(log_tx: Option<Sender<Suggestion>>, uuid: Option<uuid::Uuid>) -> FixmeCmd;
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Suggestion {
    /// Custom Rendered
    pub rendered: String,
    pub message: String,
    pub src_path: String,
    pub spans: Vec<CompilerMessageSpan>,
    pub level: String,
    pub code: Code,
    pub state: SuggestionState,
    pub children: Vec<ClippyChildren>,
    pub tool: String,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct SuggestionState {
    pub collapsed: bool,
}
