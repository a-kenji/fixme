use crate::cli::CliArgs;
use clap::Parser;

use self::config::Config;
use self::state::State;
use self::tui::Tui;

mod bus;
mod cli;
mod config;
mod consts;
mod controller;
mod editor;
mod error;
mod integration;
mod log;
mod state;
mod tui;

#[tokio::main]
async fn main() -> Result<(), error::FixmeError> {
    let _args = CliArgs::parse();
    log::init()?;
    let _config = Config::default();
    let mut tui = Tui::init()?;
    State::init(&mut tui).await?;
    Ok(())
}
