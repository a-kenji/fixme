use std::collections::HashMap;

use crossterm::event::EventStream;
use futures::StreamExt;
use tokio::select;

use crate::bus::{ResultHandler, SystemBus};
use crate::controller::{Action, Controller};
use crate::editor::Document;
use crate::error::FixmeError;
use crate::integration::commands::actionlint::ActionLint;
use crate::integration::commands::clippy::Clippy;
use crate::integration::commands::cmd::FixmeCmd;
use crate::integration::commands::proselint::Proselint;
use crate::integration::commands::typos::Typos;
use crate::integration::queue::Queue;
use crate::tui::component::suggestion::SuggestionList;
use crate::tui::view::View;
use crate::tui::{Tui, TuiState};

pub struct State {
    bus: SystemBus,
    pub suggestions: SuggestionList,
    pub document: HashMap<String, Document>,
    pub queue: Queue,
}

impl State {
    pub(crate) fn new(bus: SystemBus) -> Self {
        Self {
            bus,
            suggestions: SuggestionList::default(),
            document: HashMap::new(),
            queue: Queue::default(),
        }
    }

    pub(crate) async fn init(terminal: &mut Tui) -> Result<(), FixmeError> {
        let (mut bus, mut handler) = SystemBus::new();
        bus.init();

        let mut state = Self::new(bus);
        tracing::debug!("Initialized State");

        Self::handle(&mut state, terminal, &mut handler).await;
        Ok(())
    }
    pub(crate) async fn handle(&mut self, tui: &mut Tui, handler: &mut ResultHandler) {
        let controller = Controller::default();
        let mut reader = EventStream::new();
        let mut tui_state = TuiState::new();

        let mut controller_rx = handler.controller_rx().unwrap();
        let mut command_rx = handler.command_rx().unwrap();

        // let clippy = crate::integration::Clippy::run();
        // for message in clippy.messages {
        //     if let crate::integration::ClippyDiagnostic::Message(message) = message {
        //         let mut message: Suggestion = message.into();
        //         let file_name = message.spans.get(0).unwrap().file_name.clone();
        //         let bytes_start = message.spans.get(0).unwrap().byte_start;
        //         let bytes_end = message.spans.get(0).unwrap().byte_end;
        //         let document = Document::open(&file_name).await;
        //         let doc_slice = document.rope.get_byte_slice(bytes_start..bytes_end);
        //         tracing::error!("{:?}", doc_slice);
        //         message.rendered = doc_slice.unwrap().as_str().unwrap().to_owned();
        //         self.document.insert(file_name, document);
        //         self.suggestions.add_suggestion(message);
        //     }
        // }

        self.queue.add_job(crate::integration::queue::Pending::new(
            "Clippy".into(),
            FixmeCmd::spawn::<Clippy>(Some(self.bus.command_tx()), None).await,
        ));
        self.queue.add_job(crate::integration::queue::Pending::new(
            "Typos".into(),
            FixmeCmd::spawn::<Typos>(Some(self.bus.command_tx()), None).await,
        ));
        self.queue.add_job(crate::integration::queue::Pending::new(
            "Proselint".into(),
            FixmeCmd::spawn::<Proselint>(Some(self.bus.command_tx()), None).await,
        ));
        self.queue.add_job(crate::integration::queue::Pending::new(
            "Actionlint".into(),
            FixmeCmd::spawn::<ActionLint>(Some(self.bus.command_tx()), None).await,
        ));

        loop {
            tui.render_stateful(View::new(self), &mut tui_state);
            self.queue.poll();
            tracing::debug!("Suggestions: {:?}", self.suggestions);
            select! {

                                                        maybe_event = reader.next() => {
                                                                if let Some(event_result) = maybe_event  {
                                                tracing::debug!("Received event: {event_result:?}");
                                                match event_result {
                                                    Ok(event) => {
                                                        controller.handle(&event, self, &mut tui_state).await;
                                                    }
                                                    Err(e) => {
                                                        tracing::error!("Received error: {e:?}");
                                                    }
                                                }}}
                                                        maybe_action = controller_rx.recv() => {
                                                match maybe_action {
                                        Some(action) => { match action {
                            Action::Quit => {
                                            break
                                        }
                                        _ => {},
                        }

                                                }
                                        None => todo!(),
                                    }
                                                }
                                                        maybe_command = command_rx.recv() => {
                            match maybe_command {
                Some(suggestion) => {
                            self.suggestions.add_suggestion(suggestion);

                            }
                None => {panic!();}
            }
                }
                                            }
        }
    }

    pub(crate) fn bus(&self) -> &SystemBus {
        &self.bus
    }
}
