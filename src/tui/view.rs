mod footer;
pub mod suggestion;

use ratatui::layout::Layout;
use ratatui::widgets::StatefulWidget;
use ratatui::{buffer::Buffer, layout::Rect, widgets::Widget};

use crate::state::State;

use self::footer::Footer;

use super::theme::color::DEFAULT_BLOCK;

pub struct View<'a> {
    pub state: &'a State,
}

impl<'a> View<'a> {
    pub fn new(state: &'a State) -> Self {
        Self { state }
    }
}

impl<'a> StatefulWidget for View<'a> {
    type State = super::TuiState;
    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        let mut constraints = Vec::new();
        constraints.push(ratatui::layout::Constraint::Percentage(100));
        if state.footer {
            constraints.push(ratatui::layout::Constraint::Min(1));
        }
        let splits = Layout::default().constraints(constraints).split(area);

        DEFAULT_BLOCK.render(splits[0], buf);
        let block_inner = DEFAULT_BLOCK.inner(splits[0]);
        if !self.state.suggestions.suggestions.is_empty() {
            let suggestions = suggestion::Suggestions::new(self.state);
            suggestions.render(block_inner, buf, state.suggestions_state_mut());
        }

        if state.footer {
            let footer = Footer::new(self.state);
            footer.render(splits[1], buf);
        }
    }
}
