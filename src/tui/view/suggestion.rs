use ratatui::{
    buffer::Buffer,
    layout::{Alignment, Rect},
    style::Color,
    text::{Line, Span},
    widgets::{
        block::{title::Title, Position},
        Block, BorderType, Borders, Paragraph, Scrollbar, ScrollbarOrientation, StatefulWidget,
        Widget, Wrap,
    },
};

use crate::{
    integration::Suggestion,
    state::State,
    tui::{
        component::suggestion::SuggestionsState,
        theme::color::{DEFAULT_GRAY, DEFAULT_WIDGET_STYLE},
    },
};

pub struct Suggestions<'a> {
    state: &'a State,
}

impl<'a> Suggestions<'a> {
    pub fn new(state: &'a State) -> Self {
        Self { state }
    }
}

#[derive(Debug)]
pub struct SuggestionItem<'a> {
    suggestion: &'a Suggestion,
    focused: bool,
    collapsed: bool,
    is_rendered: bool,
}

impl<'a> SuggestionItem<'a> {
    pub fn new(suggestion: &'a Suggestion) -> Self {
        Self {
            suggestion,
            focused: false,
            collapsed: false,
            is_rendered: false,
        }
    }
    pub fn rendered_message(&self) -> &str {
        if self.is_rendered {
            self.suggestion.rendered.as_str()
        } else {
            self.suggestion.message.as_str()
        }
    }
    pub fn focused(&mut self, focused: bool) -> &mut Self {
        self.focused = focused;
        self
    }
    pub fn collapsed(&mut self, collapsed: bool) -> &mut Self {
        self.collapsed = collapsed;
        self
    }
    pub fn is_rendered(&mut self, is_rendered: bool) -> &mut Self {
        self.is_rendered = is_rendered;
        self
    }
}

const SUGGESTION_BLOCK: Block = Block::new()
    .borders(Borders::ALL)
    .border_type(BorderType::Plain);

impl<'a> Widget for SuggestionItem<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let lines: Vec<Line> = self
            .rendered_message()
            .lines()
            .map(|l| Line::from(Span::from(l)))
            .collect();

        let level_title = Title::from(format!(" [ {} ]", self.suggestion.level.clone()))
            .alignment(Alignment::Right);

        let code_title = Title::from(format!(" [ {} ]", self.suggestion.code.code.clone()))
            .alignment(Alignment::Center);

        let tool_title = Title::from(format!(" [ {} ]", self.suggestion.tool.clone()))
            .alignment(Alignment::Right)
            .position(Position::Bottom);

        let paragraph_style = if self.focused {
            DEFAULT_WIDGET_STYLE
        } else {
            DEFAULT_WIDGET_STYLE.bg(DEFAULT_GRAY)
        };

        let is_clamped = area.height < lines.len() as u16;

        let suggestion_border = if is_clamped {
            Borders::TOP | Borders::RIGHT | Borders::LEFT
        } else {
            Borders::ALL
        };
        let clamp_title = if is_clamped {
            Title::from(format!(" {}  ", crate::tui::theme::icons::FAT_ARROW_DOWN))
                .alignment(Alignment::Right)
                .position(Position::Bottom)
        } else {
            Title::from("")
        };

        let maybe_file_name = self.suggestion.spans.get(0).map(|s| s.file_name.clone());
        let file_name = maybe_file_name.unwrap_or(self.suggestion.src_path.clone());

        let mut highlight_spans: Vec<Paragraph> = vec![];
        for child in self.suggestion.children.iter() {
            for span in child.spans.iter() {
                for text in span.text.iter() {
                    // TODO: use grapheme clusters
                    let highlight: String = text
                        .text
                        .chars()
                        .skip(text.highlight_start)
                        .take(text.highlight_end.saturating_sub(text.highlight_start))
                        .collect();
                    let mut highlight = Line::from(highlight);
                    tracing::debug!("{:?}", highlight);
                    highlight.patch_style(ratatui::style::Style::default().fg(Color::Cyan));
                    highlight_spans.push(Paragraph::new(highlight));
                }
            }
        }

        let paragraph = Paragraph::new(lines)
            .alignment(Alignment::Left)
            .wrap(Wrap { trim: true })
            .block(
                SUGGESTION_BLOCK
                    .borders(suggestion_border)
                    .title(format!(" [ {} ]", file_name.clone()))
                    .title(level_title)
                    .title(code_title)
                    .title(clamp_title),
            )
            .style(paragraph_style);

        if self.collapsed {
            let lines = vec![format!(" [ {} ] ", file_name).into()];
            let line = Line::from(lines);
            let left_paragraph = Paragraph::new(line)
                .alignment(Alignment::Left)
                .style(paragraph_style);
            left_paragraph.render(area, buf);
            let lines = vec![format!(" [ {} ] ", self.suggestion.code.code).into()];
            let line = Line::from(lines);
            let center_paragraph = Paragraph::new(line)
                .alignment(Alignment::Center)
                .style(paragraph_style);
            center_paragraph.render(area, buf);
            let lines = vec![format!(" [ {} ] ", self.suggestion.level).into()];
            let line = Line::from(lines);
            let right_paragraph = Paragraph::new(line)
                .alignment(Alignment::Right)
                .style(paragraph_style);
            right_paragraph.render(area, buf);
        } else {
            paragraph.render(area, buf);
            // TODO: initial prototype
            for _highlight in highlight_spans {
                // highlight.render(area, buf);
            }
        }
    }
}

impl<'a> StatefulWidget for Suggestions<'a> {
    type State = SuggestionsState;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        let mut acc = area.y;
        let area_height = area.height;
        let suggestions = &self.state.suggestions;
        let selected = suggestions.state.selected;
        let content_height = suggestions.content_heights().iter().sum();

        let shown = self
            .state
            .suggestions
            .shown(area_height, state.offset)
            .max(1);
        if shown + state.offset <= selected + 1 {
            self.state
                .suggestions
                .focus_next_selected(area_height, &mut state.offset);
        } else if selected < state.offset {
            state.offset = selected;
        }

        for (i, suggestion) in self
            .state
            .suggestions
            .suggestions
            .iter()
            .skip(state.offset)
            .enumerate()
        {
            let len = if suggestion.state.collapsed {
                1
            } else {
                let content = if state.is_rendered {
                    &suggestion.rendered
                } else {
                    &suggestion.message
                };
                let block_height = 2;
                content
                    .lines()
                    .take((area_height.saturating_sub(acc) + 1).into())
                    .count()
                    + block_height
            };

            let truncated_height = (len as u16).min(area.height + 1 - acc);

            let rect = Rect {
                x: area.x,
                y: acc,
                width: area.width,
                height: truncated_height,
            };

            acc += len as u16;

            let mut item = SuggestionItem::new(suggestion);

            if i == self.state.suggestions.state.selected - state.offset {
                item.focused(true);
            }
            item.collapsed(suggestion.state.collapsed);
            item.is_rendered(self.state.suggestions.state.is_rendered);

            item.render(rect, buf);

            // The last item will be rendered truncated, if needed
            if acc + area.y > area.height + 1 {
                break;
            }
        }

        if content_height >= area_height.into() {
            let position = if state.offset + shown == self.state.suggestions.suggestions.len() {
                content_height
            } else {
                suggestions
                    .content_heights()
                    .iter()
                    .take(state.offset)
                    .sum()
            };
            let scrollbar = Scrollbar::new(ScrollbarOrientation::VerticalRight)
                .symbols(ratatui::symbols::scrollbar::VERTICAL)
                .track_symbol(None)
                .thumb_style(DEFAULT_WIDGET_STYLE)
                .begin_symbol(None)
                .end_symbol(None);
            let mut scroll_state = ratatui::widgets::ScrollbarState::default()
                .content_length(content_height)
                .position(position)
                .viewport_content_length(area_height.into());

            tracing::debug!(
                "Pos: {position}, Area height: {area_height}, Content height: {content_height}"
            );
            let mut scroll_area = area;
            scroll_area.x += 1;
            scrollbar.render(scroll_area, buf, &mut scroll_state);
        }
        tracing::debug!(
            "Suggestion Items {:?}",
            self.state
                .suggestions
                .get_item_bounds(selected, 1, area_height as usize)
        );
        tracing::debug!(
            "Shown Items {:?}",
            self.state.suggestions.shown(area_height, state.offset)
        );
        tracing::debug!("Selected Item {:?}", selected,);
        tracing::debug!("Offset {:?}", state.offset);
        tracing::debug!("Items {:?}", self.state.suggestions.suggestions.len());
    }
}
