use ratatui::{
    buffer::Buffer,
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    text::{Line, Span},
    widgets::{Block, Borders, Paragraph, Widget, Wrap},
};

use crate::{
    state::State,
    tui::theme::color::{
        BACKGROUND, DEFAULT_BORDER_STYLE, DEFAULT_WIDGET_STYLE, TEXT_HIGHLIGHT_STYLE,
    },
};

pub struct Footer<'a> {
    state: &'a State,
}

impl<'a> Footer<'a> {
    pub fn new(state: &'a State) -> Self {
        Self { state }
    }
}

impl Widget for Footer<'_> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let splits = Layout::default()
            .direction(Direction::Horizontal)
            .constraints(
                [
                    Constraint::Percentage(33),
                    Constraint::Percentage(33),
                    Constraint::Percentage(33),
                ]
                .as_ref(),
            )
            .split(area);

        // adjust the center area, because splits are not even
        let mut center_area = splits[1];
        center_area.x = center_area.x.saturating_sub(2);
        center_area.width = center_area.width.saturating_add(4);

        const FOOTER_STYLE: Style = DEFAULT_WIDGET_STYLE.bg(Color::Rgb(50, 50, 50));

        let _block = Block::default()
            .borders(Borders::TOP)
            .border_style(DEFAULT_BORDER_STYLE.bg(BACKGROUND))
            .style(DEFAULT_WIDGET_STYLE.bg(Color::Rgb(50, 50, 50)));

        let pending_indicator = if self.state.queue.is_pending() {
            let mut pending = String::new();
            pending.push_str(&format! {"Running: {} - ", self.state.queue.pending()});
            for job in self.state.queue.jobs() {
                pending.push_str(&format! {" {} ", job.tool});
            }
            pending
        } else {
            " ".into()
        };

        let footer_center = Paragraph::new(vec![Line::from(vec![Span::styled(
            pending_indicator,
            Style::default().fg(Color::Green).bg(Color::Rgb(50, 50, 50)),
        )])])
        .alignment(Alignment::Center)
        .wrap(Wrap { trim: true })
        .style(DEFAULT_WIDGET_STYLE.bg(Color::Rgb(50, 50, 50)));

        let footer_left = Paragraph::new(vec![Line::from(vec![
            Span::from(" "),
            Span::styled(" fixme ", TEXT_HIGHLIGHT_STYLE),
        ])])
        .alignment(Alignment::Left)
        .style(FOOTER_STYLE);

        let footer_right = Paragraph::new(vec![Line::from(vec![
            Span::styled(" ? Help ", TEXT_HIGHLIGHT_STYLE),
            Span::from(" "),
        ])])
        .alignment(Alignment::Right)
        .style(FOOTER_STYLE);

        footer_left.render(splits[0], buf);
        footer_center.render(center_area, buf);
        footer_right.render(splits[2], buf);
    }
}
