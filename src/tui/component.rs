//! Components are the stateful part of the ui.

use async_trait::async_trait;
use crossterm::event::KeyEvent;

use crate::controller::Action;
use crate::state::State;

use self::suggestion::SuggestionList;

use super::TuiState;
pub mod suggestion;

#[async_trait]
/// Routing of the currently focused component
pub trait Handler<'a> {
    async fn handle_action(
        &mut self,
        _action: &Action,
        _event: &KeyEvent,
        _state: &mut State,
        _tui: &mut TuiState,
    ) {
    }
}

#[derive(Clone, Debug)]
pub enum Component {
    SuggestionList(SuggestionList),
}

#[async_trait]
impl<'a> Handler<'a> for Component {
    async fn handle_action(
        &mut self,
        action: &Action,
        _event: &KeyEvent,
        state: &mut State,
        tui: &mut TuiState,
    ) {
        tracing::info!("Handling action {:?} for component:\n {:#?}", action, self);
        match self {
            Component::SuggestionList(suggestion_list) => {
                suggestion_list
                    .handle_action(action, _event, state, tui)
                    .await;
            }
        }
    }
}
