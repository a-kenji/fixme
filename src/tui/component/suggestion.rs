use std::fmt;

use crate::controller::{Action, Toggle};
use crate::state::State;
use crate::tui::TuiState;

use super::Handler;
use async_trait::async_trait;
use crossterm::event::KeyEvent;

#[derive(Clone)]
pub struct SuggestionList {
    pub suggestions: Vec<crate::integration::Suggestion>,
    pub state: SuggestionsState,
}

impl Default for SuggestionList {
    fn default() -> Self {
        SuggestionList::new(vec![])
    }
}

impl SuggestionList {
    pub fn new(suggestions: Vec<crate::integration::Suggestion>) -> Self {
        let state = SuggestionsState::default();
        Self { suggestions, state }
    }
    pub fn add_suggestion(&mut self, suggestion: crate::integration::Suggestion) {
        self.suggestions.push(suggestion);
    }
    pub fn content(&self) -> Vec<String> {
        let is_rendered = self.state.is_rendered;
        self.suggestions
            .iter()
            .map(|s| {
                if is_rendered {
                    s.rendered.clone()
                } else {
                    s.message.clone()
                }
            })
            .collect()
    }

    /// Height of the individual [`SuggestionItem`]
    pub fn content_heights(&self) -> Vec<usize> {
        self.suggestions
            .iter()
            .map(|s| {
                if s.state.collapsed {
                    1
                } else if self.state.is_rendered {
                    let block = 2;
                    s.rendered.lines().count() + block
                } else {
                    let block = 2;
                    s.message.lines().count() + block
                }
            })
            .collect()
    }

    pub fn get_item_bounds(
        &self,
        selected: usize,
        offset: usize,
        max_height: usize,
    ) -> (usize, usize) {
        let mut start = offset;
        let mut end = offset;
        let mut height = 0;
        for item_height in self.content_heights().iter().skip(offset) {
            if height + item_height > max_height {
                break;
            }
            height += item_height;
        }

        let selected = selected.min(self.suggestions.len() - 1);
        while selected >= end {
            height = height.saturating_add(self.content_heights()[end]);
            end += 1;
            while height > max_height {
                height = height.saturating_sub(self.content_heights()[start]);
                start += 1;
            }
        }
        while selected < start {
            start -= 1;
            height = height.saturating_add(self.content_heights()[start]);
            while height > max_height {
                end -= 1;
                height = height.saturating_sub(self.content_heights()[end]);
            }
        }

        (start, end)
    }
    // How many items will be shown in a given height
    pub fn shown(&self, area_height: u16, offset: usize) -> usize {
        let mut acc = 0;
        let mut amount = 0;
        for height in self.content_heights().iter().skip(offset) {
            if acc + *height as u16 >= area_height {
                return amount;
            }
            amount += 1;
            acc += *height as u16;
        }
        amount
    }
    /// Shift the selected item into full focus, if possible
    /// TODO: needs special handling, for small windows
    pub fn focus_next_selected(&self, area_height: u16, offset: &mut usize) {
        let mut guard = 0;
        loop {
            let mut acc = 0;
            for height in self.content_heights().iter().skip(*offset).take(
                self.state
                    .selected
                    .saturating_sub(*offset)
                    .saturating_add(1),
            ) {
                acc += *height as u16;
                tracing::debug!("Height: {height}");
            }
            if acc >= area_height {
                *offset = offset.saturating_add(1);
            } else {
                return;
            }

            if guard >= 10 {
                break;
            } else {
                guard += 1;
            }
        }
    }

    pub fn next(&mut self) {
        self.state.selected = usize::min(
            self.state.selected.saturating_add(1),
            self.suggestions.len() - 1,
        );
    }
    pub fn previous(&mut self) {
        self.state.selected = self.state.selected.saturating_sub(1);
    }
    pub fn toggle_collapsed(&mut self) {
        let index = self.state.selected;
        if let Some(selected) = self.suggestions.get_mut(index) {
            selected.state.collapsed = !selected.state.collapsed;
        }
    }
}

#[derive(Debug, Clone, Default)]
pub struct SuggestionsState {
    pub offset: usize,
    pub selected: usize,
    pub is_rendered: bool,
}

impl SuggestionsState {
    pub fn toggle_rendered(&mut self) {
        self.is_rendered = !self.is_rendered;
    }
}

#[async_trait]
impl Handler<'_> for SuggestionList {
    async fn handle_action(
        &mut self,
        action: &Action,
        _event: &KeyEvent,
        state: &mut State,
        tui: &mut TuiState,
    ) {
        match action {
            Action::Previous => {
                state.suggestions.previous();
            }
            Action::Next => {
                state.suggestions.next();
            }
            Action::Toggle(Toggle::Collapsed) => {
                state.suggestions.toggle_collapsed();
            }
            Action::Toggle(Toggle::Rendered) => {
                tui.suggestions_state.toggle_rendered();
            }
            #[allow(unreachable_patterns)]
            _ => {
                tracing::debug!("Unhandled action: {action:?}");
            }
        }
    }
}

impl fmt::Debug for SuggestionList {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        let focused = &self.suggestions.get(self.state.selected);
        fmt.debug_struct("SuggestionList")
            .field("suggestions", &self.suggestions.len())
            .field("focused", focused)
            .field("state", &self.state)
            .finish()
    }
}
