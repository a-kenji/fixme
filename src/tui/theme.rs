#![allow(dead_code)]

pub(crate) mod icons {
    pub(crate) const FAT_ARROW_DOWN: &str = "";
    pub(crate) const SMALL_ARROW_DOWN: &str = "⌄";
}

pub(crate) mod color {
    use ratatui::style::{Color, Modifier, Style};
    use ratatui::widgets::{Block, BorderType, Borders};

    pub(crate) const BACKGROUND: Color = Color::Reset;
    pub(crate) const FOREGROUND: Color = Color::Gray;
    pub(crate) const DEFAULT_GRAY: Color = Color::Rgb(50, 50, 50);
    pub(crate) const BORDER_TYPE: BorderType = BorderType::Rounded;

    pub(crate) const DEFAULT_WIDGET_STYLE: Style = Style::new().bg(BACKGROUND).fg(FOREGROUND);
    pub(crate) const DEFAULT_BLOCK: Block = Block::new()
        .borders(Borders::ALL)
        .border_type(BorderType::Thick);
    pub(crate) const DEFAULT_BORDER_STYLE: Style = Style::new().fg(Color::DarkGray);
    pub(crate) static TEXT_HIGHLIGHT_STYLE: Style = Style::new()
        .bg(Color::DarkGray)
        .fg(Color::Gray)
        .add_modifier(Modifier::BOLD);
}
