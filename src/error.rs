use thiserror::Error;

/// TODO: Break errors up into their own domains.

#[derive(Debug, Error)]
pub enum FixmeError {
    #[error("IoError: {0}")]
    Io(#[from] std::io::Error),
    #[error("Deserialization Toml Error: {0}")]
    TomlDeSerde(#[from] toml::de::Error),
    #[error("Serialization Toml Error: {0}")]
    TomlSerSerde(#[from] toml::ser::Error),
    #[error("Ron Error: {0}")]
    Ron(#[from] ron::Error),
    #[error("JoinError: {0}")]
    Join(#[from] tokio::task::JoinError),
    // #[error("Utf8 Conversion Error")]
    // Utf8(#[from] std::str::Utf8Error),
}
