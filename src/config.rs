use serde::{Deserialize, Serialize};
use thiserror::Error;

const DEFAULT_CONFIG_FILE_NAME: &str = ".fixme.toml";
const DEFAULT_CONFIG_FILE_NAME_ALT: &str = "fixme.toml";
const DEFAULT_CONFIG_FILE: &str = include_str!("config/fixme.toml");

type ConfigResult = Result<Config, ConfigError>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Config {}

impl Config {
    pub fn try_from_toml(data: &str) -> ConfigResult {
        match toml::from_str(data) {
            Ok(config) => {
                tracing::info!("Deserialized configuration {config:#?}");
                Ok(config)
            }
            Err(e) => {
                tracing::error!("Could not deserialize configuration: {e}");
                Err(e.into())
            }
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        // If this fails, then there is an error in the parsing of the configuration file,
        // or the default configuration file itself.
        Self::try_from_toml(DEFAULT_CONFIG_FILE)
            .expect("There is an error in the default configuration file.")
    }
}

#[derive(Error, Debug)]
pub enum ConfigError {
    // Io error
    #[error("IoError: {0}")]
    Io(#[from] std::io::Error),
    #[error("Config error: {0}")]
    Std(#[from] Box<dyn std::error::Error>),
    // Io error with path context
    // #[error("IoError: {0}, File: {1}")]
    // IoPath(std::io::Error, std::path::PathBuf),
    // Internal deserialization error
    #[error("FromUtf8Error: {0}")]
    FromUtf8(#[from] std::string::FromUtf8Error),
    // Deserialization Error
    #[error("FromRonError: {0}")]
    Ron(#[from] ron::error::Error),
    // Deserialization With Span Error
    #[error("FromRonSpannedError: {0}")]
    RonSpanned(#[from] ron::error::SpannedError),
    #[error("Deserialization Toml Error: {0}")]
    TomlDeSerde(#[from] toml::de::Error),
    #[error("Serialization Toml Error: {0}")]
    TomlSerSerde(#[from] toml::ser::Error),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_default_configuration() {
        let _ = Config::default();
    }
    #[test]
    fn test_default_configuration_unwrap() {
        Config::try_from_toml(DEFAULT_CONFIG_FILE)
            .expect("There is an error in the default configuration file.");
    }
    #[test]
    #[should_panic]
    fn test_false_config_should_panic() {
        Config::try_from_toml("Not valid configuration.")
            .expect("There is an error in the default configuration file.");
    }
}
