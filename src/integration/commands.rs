pub(crate) mod actionlint;
pub(crate) mod clippy;
pub(crate) mod proselint;
pub(crate) mod typos;

pub mod cmd {
    use tokio::{
        io::{AsyncBufReadExt, AsyncReadExt, BufReader},
        process::Command,
        sync::mpsc::{self, Receiver, Sender},
    };

    use crate::{
        error::FixmeError,
        integration::{CmdIntegration, Suggestion},
    };

    type FixmeResult<T> = Result<T, FixmeError>;

    #[derive(Debug)]
    pub struct FixmeCmd {
        cmd: String,
        args: Vec<String>,
        log_tx: Option<Sender<Suggestion>>,
        uuid: Option<uuid::Uuid>,
    }

    pub struct FixmeLogger {
        log_rx: Receiver<Suggestion>,
    }

    impl FixmeLogger {
        pub fn new(log_rx: Receiver<Suggestion>) -> Self {
            Self { log_rx }
        }
    }

    impl FixmeCmd {
        pub fn new(cmd: String, args: Vec<String>, uuid: Option<uuid::Uuid>) -> Self {
            Self {
                args,
                uuid,
                log_tx: None,
                cmd,
            }
        }
        /// Spawn the command on a dedicated thread.
        pub async fn spawn<I>(
            log_tx: Option<Sender<Suggestion>>,
            uuid: Option<uuid::Uuid>,
        ) -> tokio::task::JoinHandle<Result<String, ()>>
        where
            I: CmdIntegration,
        {
            tokio::spawn(async move {
                let cmd = I::cmd(log_tx, uuid);
                cmd.run::<I>()
                    .await
                    .map_err(|e| tracing::error!("Error running command {cmd:?}: {e}"))
            })
        }
        /// Creates a new `[FixmeCmd]`,
        /// and optionally connects a `[Sender<NixLog>]`
        /// with an optional uuid
        pub fn init(
            cmd: String,
            args: Vec<String>,
            log_tx: Option<Sender<Suggestion>>,
            uuid: Option<uuid::Uuid>,
        ) -> Self {
            let mut cmd = FixmeCmd::new(cmd, args, uuid);
            if let Some(log_tx) = log_tx {
                let mut logger = cmd.init_log();
                tokio::spawn(async move {
                    while let Some(msg) = logger.log_rx.recv().await {
                        if let Err(e) = log_tx.send(msg).await {
                            // This should only happen on shutdown of the application.
                            // When the channel disconnected.
                            tracing::error!(
                                "Error from log sender: {e}\nCleaning up logger thread."
                            );
                            break;
                        }
                    }
                });
            }
            cmd
        }
        pub fn init_log(&mut self) -> FixmeLogger {
            let (tx, rx) = mpsc::channel(24);
            self.set_log_tx(Some(tx));
            FixmeLogger::new(rx)
        }
        fn log_tx(&self) -> Option<mpsc::Sender<Suggestion>> {
            self.log_tx.clone()
        }

        async fn run<I>(&self) -> FixmeResult<String>
        where
            I: CmdIntegration,
        {
            let mut child = Command::new(&self.cmd)
                .args(self.args())
                .kill_on_drop(true)
                .stdin(std::process::Stdio::null())
                .stdout(std::process::Stdio::piped())
                .stderr(std::process::Stdio::piped())
                .spawn()?;

            tracing::debug!("Args: {:?}", self.args);
            tracing::debug!("Running: {:?}", child);

            // TODO: make configurable from which line is read/stdout or stdin.
            let stdout = child.stdout.take().expect("Couldn't take stdout.");
            let stderr = child.stderr.take().expect("Couldn't take stderr.");

            let maybe_log_tx = self.log_tx();
            let maybe_uuid = self.uuid().copied();
            let stderr_task = tokio::spawn(async move {
                let mut reader = BufReader::new(stdout);
                let mut line = String::new();
                loop {
                    line.clear();
                    match reader.read_line(&mut line).await {
                        Err(e) => {
                            tracing::error!("Err while reading stderr: {e}");
                        }
                        Ok(0) => {
                            // NOTE: If there is an empty line we assume
                            // that the command has terminated.
                            // Is this assumption robust?
                            tracing::debug!("End of nix command stderr.");
                            tracing::debug!("Command has terminated.");
                            return Ok(());
                        }
                        Ok(_) => {
                            if let Some(log_tx) = maybe_log_tx.clone() {
                                if let Some(suggestion) = I::from_line(&line) {
                                    tracing::error!("Suggestion: {suggestion:?}");
                                    // let mut nix_log: NixLog = serde_json::from_str(&log).unwrap();
                                    // nix_log.set_uuid(maybe_uuid);
                                    // }
                                    if let Err(e) = log_tx.send(suggestion).await {
                                        // This should only happen on channel disconnect.
                                        tracing::error!(
                                        "Error from log sender: {e}\nShutting down the log reader."
                                    );
                                        return Ok(());
                                    }
                                }
                            }
                        }
                    }
                }
            });

            let mut reader = BufReader::new(stderr);
            let mut buf = String::new();
            reader.read_to_string(&mut buf).await?;

            child.wait().await?;
            let _: core::result::Result<(), ()> = stderr_task.await?;

            Ok(buf)
        }

        pub fn set_log_tx(&mut self, log_tx: Option<Sender<Suggestion>>) {
            self.log_tx = log_tx;
        }

        pub fn args(&self) -> &[String] {
            self.args.as_ref()
        }

        pub fn uuid(&self) -> Option<&uuid::Uuid> {
            self.uuid.as_ref()
        }
    }
}
