use serde::{Deserialize, Serialize};
use tokio::sync::mpsc::Sender;

use crate::integration::{CmdIntegration, Suggestion};

use super::cmd::FixmeCmd;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Typos {}

impl CmdIntegration for Typos {
    fn from_line(line: &str) -> Option<Suggestion> {
        tracing::debug!("Parsing line:\n{line:?}");
        match serde_json::from_str::<TypoMessage>(line) {
            Ok(diagnostic) => {
                tracing::debug!("Successfully parsed the typos message:\n{diagnostic:?}");
                Some(diagnostic.into())
            }
            Err(e) => {
                tracing::error!("failed to parse typos stdout: {e:?}");
                tracing::error!("\n for the following message: {line:?}");
                None
            }
        }
    }

    fn cmd(log_tx: Option<Sender<Suggestion>>, uuid: Option<uuid::Uuid>) -> FixmeCmd {
        let cmd = "typos";
        let args = vec!["--format", "json"];
        let args = args.into_iter().map(|a| a.to_owned()).collect();
        FixmeCmd::init(cmd.into(), args, log_tx, uuid)
    }
}

impl Typos {
    pub fn new() -> Self {
        Default::default()
    }
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TypoMessage {
    #[serde(rename = "type")]
    pub type_field: String,
    pub path: String,
    #[serde(rename = "line_num")]
    pub line_num: i64,
    #[serde(rename = "byte_offset")]
    pub byte_offset: i64,
    pub typo: String,
    pub corrections: Vec<String>,
}

impl From<TypoMessage> for Suggestion {
    fn from(value: TypoMessage) -> Self {
        let code = super::clippy::Code {
            code: value.type_field,
            explanation: None,
        };
        Self {
            message: format!("{}\n{:?}", value.typo.clone(), value.corrections.clone()),
            rendered: format!("{}\n{:?}", value.typo.clone(), value.corrections.clone()),
            src_path: value.path,
            code,
            tool: "typos".into(),
            ..Default::default()
        }
    }
}
