use serde::{Deserialize, Serialize};
use tokio::sync::mpsc::Sender;

use crate::integration::{CmdIntegration, Suggestion};

use super::cmd::FixmeCmd;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Proselint {}

impl CmdIntegration for Proselint {
    fn from_line(line: &str) -> Option<Suggestion> {
        tracing::debug!("Parsing line:\n{line:?}");
        match serde_json::from_str::<ProselintDiagnostic>(line) {
            Ok(diagnostic) => {
                tracing::debug!("Successfully parsed the typos message:\n{diagnostic:?}");
                Some(diagnostic.into())
            }
            Err(e) => {
                tracing::error!("failed to parse typos stdout: {e:?}");
                tracing::error!("\n for the following message: {line:?}");
                None
            }
        }
    }

    fn cmd(log_tx: Option<Sender<Suggestion>>, uuid: Option<uuid::Uuid>) -> FixmeCmd {
        let cmd = "proselint";
        let args = vec!["--json", "README.md"];
        let args = args.into_iter().map(|a| a.to_owned()).collect();
        FixmeCmd::init(cmd.into(), args, log_tx, uuid)
    }
}

impl Proselint {
    pub fn new() -> Self {
        Default::default()
    }
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProselintDiagnostic {
    pub data: Data,
    pub status: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Data {
    pub errors: Vec<Error>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Error {
    pub check: String,
    pub column: i64,
    pub end: i64,
    pub extent: i64,
    pub line: i64,
    pub message: String,
    // TODO: check replacement suggestions
    pub replacements: Option<String>,
    pub severity: String,
    pub start: i64,
}

// TODO: handle into Vec<Suggestion>
impl From<ProselintDiagnostic> for Suggestion {
    fn from(value: ProselintDiagnostic) -> Self {
        Self {
            message: format!("{:?}", value.data.errors.clone()),
            // rendered: format!("{}\n{:?}", value.typo.clone(), value.corrections.clone()),
            src_path: "README.md".into(),
            tool: "proselint".into(),
            ..Default::default()
        }
    }
}
