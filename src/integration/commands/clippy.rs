use serde::{Deserialize, Serialize};
use tokio::sync::mpsc::Sender;

use crate::integration::{CmdIntegration, Suggestion};

use super::cmd::FixmeCmd;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Clippy {}

impl CmdIntegration for Clippy {
    fn from_line(line: &str) -> Option<Suggestion> {
        tracing::error!("Parsing line:\n{line:?}");
        match serde_json::from_str::<ClippyDiagnostic>(line) {
            Ok(diagnostic) => {
                if let ClippyDiagnostic::Message(message) = diagnostic {
                    tracing::error!("Successfully parsed the clippy message:\n{message:?}");
                    Some(message.into())
                } else {
                    None
                }
            }
            Err(e) => {
                tracing::error!("failed to parse clippy stdout: {e:?}");
                tracing::error!("\n for the following message: {line:?}");
                None
            }
        }
    }

    fn cmd(log_tx: Option<Sender<Suggestion>>, uuid: Option<uuid::Uuid>) -> FixmeCmd {
        let cmd = "cargo";
        let args = vec!["clippy", "--message-format", "json"];
        let args = args.into_iter().map(|a| a.to_owned()).collect();
        FixmeCmd::init(cmd.into(), args, log_tx, uuid)
    }
}

impl From<CompilerMessage> for Suggestion {
    fn from(value: CompilerMessage) -> Self {
        Self {
            rendered: String::new(),
            message: value.message.rendered,
            src_path: value.target.src_path,
            spans: value.message.spans,
            level: value.message.level,
            code: value.message.code,
            children: value.message.children,
            tool: "cargo clippy".into(),
            ..Default::default()
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "reason")]
pub enum ClippyDiagnostic {
    #[serde(rename = "compiler-message")]
    Message(CompilerMessage),
    #[serde(rename = "compiler-artifact")]
    Artifact {},
    #[serde(rename = "build-finished")]
    Finished {},
    #[serde(rename = "build-script-executed")]
    BuildScript {},
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct CompilerMessage {
    message: ClippyMessage,
    target: Target,
    manifest_path: String,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct ClippyMessage {
    rendered: String,
    // manifest_path: String,
    // TODO: parse the package id
    // package_id: String,
    // target: Target,
    children: Vec<ClippyChildren>,
    code: Code,
    level: String,
    message: String,
    pub spans: Vec<CompilerMessageSpan>,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Code {
    pub code: String,
    pub explanation: Option<String>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Target {
    src_path: String,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct ClippyChildren {
    code: Option<String>,
    level: Option<String>,
    message: Option<String>,
    pub spans: Vec<CompilerMessageSpan>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Spans {}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct CompilerMessageSpan {
    pub byte_end: usize,
    pub byte_start: usize,
    column_end: usize,
    column_start: usize,
    expansion: Option<String>,
    pub file_name: String,
    is_primary: bool,
    label: Option<String>,
    line_end: usize,
    line_start: usize,
    suggested_replacement: Option<String>,
    suggestion_applicability: Option<String>,
    pub text: Vec<CompilerMessageSpanText>,
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct CompilerMessageSpanText {
    pub highlight_end: usize,
    pub highlight_start: usize,
    pub text: String,
}
