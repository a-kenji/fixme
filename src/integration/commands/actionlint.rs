use serde::{Deserialize, Serialize};
use tokio::sync::mpsc::Sender;

use crate::integration::{CmdIntegration, Suggestion};

use super::cmd::FixmeCmd;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct ActionLint {}

impl CmdIntegration for ActionLint {
    fn from_line(line: &str) -> Option<Suggestion> {
        tracing::error!("Parsing line:\n{line:?}");
        match serde_json::from_str::<ActionLintDiagnostic>(line) {
            Ok(diagnostic) => {
                tracing::error!("Successfully parsed the clippy message:\n{diagnostic:?}");
                Some(diagnostic.into())
            }
            Err(e) => {
                tracing::error!("failed to parse clippy stdout: {e:?}");
                tracing::error!("\n for the following message: {line:?}");
                None
            }
        }
    }

    fn cmd(log_tx: Option<Sender<Suggestion>>, uuid: Option<uuid::Uuid>) -> FixmeCmd {
        let cmd = "cargo";
        let args = vec!["clippy", "--message-format", "json"];
        let args = args.into_iter().map(|a| a.to_owned()).collect();
        FixmeCmd::init(cmd.into(), args, log_tx, uuid)
    }
}

impl From<ActionLintDiagnostic> for Suggestion {
    fn from(value: ActionLintDiagnostic) -> Self {
        Self {
            rendered: String::new(),
            message: format!("{:?}", value.first()),
            // src_path: value.target.src_path,
            // spans: value.message.spans,
            // level: value.message.level,
            // code: value.message.code,
            // children: value.message.children,
            tool: "actionlint".into(),
            ..Default::default()
        }
    }
}

pub type ActionLintDiagnostic = Vec<ActionLintItem>;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ActionLintItem {
    pub message: String,
    pub filepath: String,
    pub line: i64,
    pub column: i64,
    pub kind: String,
    pub snippet: String,
    #[serde(rename = "end_column")]
    pub end_column: i64,
}
