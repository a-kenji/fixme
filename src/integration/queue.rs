use tokio::task::JoinHandle;
use uuid::Uuid;

#[derive(Default)]
pub struct Queue {
    jobs: Vec<Pending>,
}

impl Queue {
    pub fn add_job(&mut self, job: Pending) {
        self.jobs.push(job);
    }
    /// Check if any of the pending jobs have finished.
    pub fn poll(&mut self) {
        self.jobs.retain_mut(|j| !j.handle.is_finished());
    }
    pub fn pending(&self) -> usize {
        self.jobs.len()
    }
    pub fn jobs(&self) -> &[Pending] {
        &self.jobs
    }
    pub fn is_pending(&self) -> bool {
        !self.jobs.is_empty()
    }
}

pub struct Pending {
    // _uuid: Uuid,
    pub tool: String,
    handle: JoinHandle<Result<String, ()>>,
}

impl Pending {
    pub fn new(tool: String, handle: JoinHandle<Result<String, ()>>) -> Self {
        Self { tool, handle }
    }
}
