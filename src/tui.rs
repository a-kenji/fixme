pub mod component;
mod theme;
pub(crate) mod view;

use std::io::Stdout;

use crossterm::execute;
use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen, SetTitle,
};
use ratatui::backend::CrosstermBackend;
use ratatui::widgets::{StatefulWidget, Widget};
use ratatui::Terminal;

use crate::error::FixmeError;

/// A wrapper struct around tui
pub(crate) struct Tui {
    pub(crate) terminal: Terminal<CrosstermBackend<Stdout>>,
}

impl Tui {
    pub(crate) fn new(terminal: Terminal<CrosstermBackend<Stdout>>) -> Self {
        Self { terminal }
    }

    pub(crate) fn init() -> Result<Self, FixmeError> {
        enable_raw_mode()?;
        let mut stdout = std::io::stdout();
        execute!(stdout, EnterAlternateScreen)?;
        let title = "fixme";
        crossterm::execute!(stdout, SetTitle(title))?;
        let backend = CrosstermBackend::new(stdout);
        let terminal = Terminal::new(backend)?;
        Ok(Self::new(terminal))
    }

    pub(crate) fn exit(&mut self) -> Result<(), FixmeError> {
        disable_raw_mode()?;
        execute!(self.terminal.backend_mut(), LeaveAlternateScreen,)?;
        self.terminal.show_cursor()?;
        Ok(())
    }

    pub(crate) fn render_stateful<W>(&mut self, widget: W, state: &mut W::State)
    where
        W: StatefulWidget,
    {
        self.terminal
            .draw(|frame| {
                let rect = frame.size();
                frame.render_stateful_widget(widget, rect, state);
            })
            .ok();
    }
}

#[derive(Debug, Default)]
pub struct TuiState {
    suggestions_state: component::suggestion::SuggestionsState,
    /// Whether the footer is visible
    footer: bool,
}

impl TuiState {
    pub fn new() -> Self {
        Self {
            footer: true,
            ..Default::default()
        }
    }

    pub fn suggestions_state_mut(&mut self) -> &mut component::suggestion::SuggestionsState {
        &mut self.suggestions_state
    }
    pub fn toggle_footer(&mut self) {
        self.footer = !self.footer;
    }
}

impl Drop for Tui {
    fn drop(&mut self) {
        self.exit()
            .unwrap_or_else(|e| tracing::error!("Error trying to exit the terminal: {e}"));
        tracing::debug!("Dropped Tui");
    }
}
