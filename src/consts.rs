/// Default path used for logging
pub const LOG_PATH: &str = "/tmp/fixme/fixme.log";
/// Key of the logging directive for fixme
pub const LOG_ENV: &str = "FIXME_LOG";
