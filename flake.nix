{
  description = "A friendly lint and lsp helper";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    rust-overlay,
    crane,
  }:
    flake-utils.lib.eachDefaultSystem
    (system: let
      pkgs = nixpkgs.legacyPackages.${system};
      stdenv =
        if pkgs.stdenv.isLinux
        then pkgs.stdenvAdapters.useMoldLinker pkgs.stdenv
        else pkgs.stdenv;
      overlays = [(import rust-overlay)];
      rustPkgs = import nixpkgs {
        inherit system overlays;
      };
      src = self;
      RUST_TOOLCHAIN = src + "/rust-toolchain.toml";
      RUSTFMT_TOOLCHAIN = src + "/.rustfmt-toolchain.toml";

      cargoTOML = builtins.fromTOML (builtins.readFile (src + "/Cargo.toml"));
      inherit (cargoTOML.package) version name;
      # rustToolchainTOML = rustPkgs.rust-bin.fromRustupToolchainFile RUST_TOOLCHAIN;
      rustToolchainTOML = rustPkgs.rust-bin.stable.latest.minimal;

      rustFmtToolchainTOML = rustPkgs.rust-bin.fromRustupToolchainFile RUSTFMT_TOOLCHAIN;

      rustToolchainDevTOML = rustToolchainTOML.override {
        extensions = ["rustfmt" "clippy" "rust-analysis" "rust-docs"];
        targets = [];
      };
      gitDate = "${builtins.substring 0 4 self.lastModifiedDate}-${builtins.substring 4 2 self.lastModifiedDate}-${builtins.substring 6 2 self.lastModifiedDate}";
      gitRev = self.shortRev or "Not committed yet.";
      cargoLock = {
        lockFile = builtins.path {
          path = self + "/Cargo.lock";
          name = "Cargo.lock";
        };
        allowBuiltinFetchGit = true;
      };
      rustc = rustToolchainTOML;
      cargo = rustToolchainTOML;

      devInputs = [
        rustToolchainDevTOML
        rustFmtToolchainTOML
        # pkgs.just
        # pkgs.lychee
        pkgs.cargo-watch
        pkgs.cargo-tarpaulin
        #
        # pkgs.cargo-deny
        # pkgs.cargo-bloat
        # pkgs.cargo-machete
        # pkgs.cargo-outdated
        # pkgs.cargo-flamegraph
        # pkgs.cargo-diet
        # pkgs.cargo-modules
        # pkgs.cargo-nextest
        # pkgs.cargo-dist
        # pkgs.cargo-public-api
        # pkgs.cargo-unused-features
        #
        # # snapshot testing
        # pkgs.cargo-insta
        #
        # (pkgs.symlinkJoin {
        #   name = "cargo-udeps-wrapped";
        #   paths = [pkgs.cargo-udeps];
        #   nativeBuildInputs = [pkgs.makeWrapper];
        #   postBuild = ''
        #     wrapProgram $out/bin/cargo-udeps \
        #       --prefix PATH : ${pkgs.lib.makeBinPath [
        #       (rustPkgs.rust-bin.selectLatestNightlyWith
        #         (toolchain: toolchain.default))
        #     ]}
        #   '';
        # })
        # (pkgs.symlinkJoin {
        #   name = "cargo-careful-wrapped";
        #   paths = [pkgs.cargo-careful];
        #   nativeBuildInputs = [pkgs.makeWrapper];
        #   postBuild = ''
        #     wrapProgram $out/bin/cargo-careful \
        #       --prefix PATH : ${pkgs.lib.makeBinPath [
        #       (rustPkgs.rust-bin.selectLatestNightlyWith
        #         (
        #           toolchain:
        #             toolchain
        #             .default
        #             .override {
        #               extensions = ["rust-src"];
        #             }
        #         ))
        #     ]}
        #   '';
        # })
        #alternative linker
        pkgs.clang
      ];
      shellInputs = [
        # pkgs.shellcheck
        # pkgs.actionlint
      ];
      fmtInputs = [
        # pkgs.alejandra
        # pkgs.treefmt
        # pkgs.typos
      ];
      editorConfigInputs = [
        # pkgs.editorconfig-checker
      ];
      actionlintInputs = [
        # pkgs.actionlint
      ];
      # Common arguments for the crane build
      commonArgs = {
        inherit stdenv version name;
        pname = name;
        src = pkgs.lib.cleanSourceWith {
          src = craneLib.path ./.; # The original, unfiltered source
        };
      };
      craneLib = (crane.mkLib pkgs).overrideToolchain rustToolchainTOML;
      # Build *just* the cargo dependencies, so we can reuse
      # all of that work (e.g. via cachix) when running in CI
      cargoArtifacts = craneLib.buildDepsOnly commonArgs;
      meta = with pkgs.lib; {
        homepage = "https://github.com/a-kenji/fixme";
        description = "A friendly lint and lsp helper";
        license = [licenses.mit];
      };
    in rec {
      devShells = {
        default = devShells.fullShell;
        fullShell = (pkgs.mkShell.override {inherit stdenv;}) {
          buildInputs = shellInputs ++ fmtInputs ++ devInputs;
          inherit name;
          RUST_LOG = "debug";
          RUST_BACKTRACE = true;
          # RUSTFLAGS = "-C linker=clang -C link-arg=-fuse-ld=${pkgs.mold}/bin/mold -C target-cpu=native";
          RUSTFLAGS = "-C linker=clang -C link-arg=-fuse-ld=${pkgs.mold}/bin/mold";
        };
        editorConfigShell = pkgs.mkShell {
          buildInputs = editorConfigInputs;
        };
        actionlintShell = pkgs.mkShell {
          buildInputs = actionlintInputs;
        };
        fmtShell = pkgs.mkShell {
          buildInputs = fmtInputs;
        };
      };
      packages = {
        default = packages.crane;
        upstream =
          (
            pkgs.makeRustPlatform {
              inherit cargo rustc;
            }
          )
          .buildRustPackage {
            cargoDepsName = name;
            GIT_DATE = gitDate;
            GIT_REV = gitRev;
            doCheck = false;
            version = "unstable" + gitDate;
            inherit
              name
              src
              stdenv
              cargoLock
              meta
              ;
          };
        crane = craneLib.buildPackage (commonArgs
          // {
            cargoExtraArgs = "-p ${name}";
            GIT_DATE = gitDate;
            GIT_REV = gitRev;
            doCheck = false;
            version = "unstable-" + gitDate;
            pname = name;
            inherit
              cargoArtifacts
              meta
              name
              stdenv
              ;
          });
      };
      apps.default = {
        type = "app";
        program = "${packages.default}/bin/${name}";
      };
      formatter = pkgs.alejandra;
    });
}
