{
  mkShell,
  buildInputs,
  nativeBuildInputs,
}:
mkShell {
  name = "fixme-dev-env";
  inherit buildInputs nativeBuildInputs;
  ### Environment Variables
  RUST_BACKTRACE = 1;
}
